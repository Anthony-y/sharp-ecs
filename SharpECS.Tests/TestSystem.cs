﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpECS;
using Microsoft.Xna.Framework;

namespace SharpECS.Tests
{
    public class TestSystem : EntitySystem<TestComponent>
    {
        public TestSystem(EntityWorld entityWorld)
            : base(entityWorld)
        {
            foreach (var i in CompatibleEntities)
            {
                Console.WriteLine("Compatible Entities of TestSystem: " + i.Tag);
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var i in CompatibleEntities)
            {
                Console.WriteLine(i.FindComponent<TestComponent>().Owner.Tag);
            }

            base.Update(gameTime);
        }

    }
}
