﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SharpECS;

namespace SharpECS.Tests
{
    public class TestComponent : IComponent
    {
        public IEntity Owner { get; set; }
        public string Tag { get; set; }

        public TestComponent(IEntity owner) { Owner = owner; }
        public TestComponent(EntityWorld ew, string tag)
        {
            var entity = ew.GetEntity(tag);
            if (entity != null)
            {
                Owner = entity;
            } else { Console.WriteLine("Could not create TestComponent. Could not find entity \"" + tag + "\""); }
        }

    }
}
