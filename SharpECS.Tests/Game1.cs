﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using SharpECS;

namespace SharpECS.Tests
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        EntityWorld ew;
        IEntity e;
        IEntity p;
        TestSystem sys;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            e = new BaseEntity("Player");
            p = new BaseEntity("Enemy");
            ew = new EntityWorld();
            ew.AddEntity(e);
            ew.AddEntity(p);
            ew.AddComponent(e, new TestComponent(e));
            ew.AddComponent(p, new TestComponent(p));
            sys = new TestSystem(ew);

            foreach (var i in sys.CompatibleEntities) { Console.WriteLine(i.Tag); }
           
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) { Exit(); }

            sys.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            sys.Update(gameTime);

            base.Draw(gameTime);
        }
    }
}
