﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpECS
{
    /// <summary>
    /// Base interface for all components.
    /// </summary>
    public interface IComponent
    {
        // This is a very powerful thing.
        // It allows a component to know about it's owner's other components.
        IEntity Owner { get; set; }
        
        // Can be used when C# types are no use.
        // For example, for use with IronPython.
        string Tag { get; set; }
    }
}
