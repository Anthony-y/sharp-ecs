﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpECS
{
    /// <summary>
    /// The class responsible for managing entities
    /// and allowing their components to be used with EntitySystems.
    /// IF AN ENTITY IS NOT REGISTERED UNDER AN INSTANCE OF ENTITY WORLD,
    /// ENTITY SYSTEM WILL NOT FIND IT OR IT'S COMPONENTS.
    /// </summary>
    public class EntityWorld
    {
        internal List<IEntity> Entities { get; set; }

        public EntityWorld()
        {
            Entities = new List<IEntity>();
        }

        /// <summary>
        /// Creates a new BASE entity and adds it to the world.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public BaseEntity CreateEntity(string tag)
        {
            var newEntity = new BaseEntity(tag);
            Entities.Add(newEntity);
            return newEntity;
        }
        
        /// <summary>
        /// Adds an EXISTING entity to the world.
        /// This is useful for Entities which are NOT BASE ENTITIES.
        /// </summary>
        /// <param name="entity"></param>
        public void AddEntity(IEntity entity) { Entities.Add(entity); }

        /// <summary>
        /// A wrapper around IEntity's AddComponent.
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="c"></param>
        public void AddComponent(string tag, IComponent c)
        {
            var entity = GetEntity(tag);

            // These checks make sure that the Components owner
            // (passed into the contructor of a component)
            // is the same as the entity of whom's tag you pass into this function.
            if (c.Owner == entity) { entity.AddComponent(c); }
            else { c.Owner = entity; entity.AddComponent(c); }
        }

        /// <summary>
        /// Straight up wrapper around AddComponent
        /// except takes in an IEntity directly instead of a tag.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="c"></param>
        public void AddComponent(IEntity e, IComponent c) { e.AddComponent(c); }

        /// <summary>
        /// Returns the first entity it finds with the specfifed tag.
        /// If it doesn't find anything, it returns null.
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public IEntity GetEntity(string tag)
        {
            var match = Entities.FirstOrDefault(ent => ent.Tag == tag);
            if (match != null) { return match; }
            else { Console.WriteLine("Could not return Entity. Entity (probably) does not exist."); return null; }
        }
    }
}
