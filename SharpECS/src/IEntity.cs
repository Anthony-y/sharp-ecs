﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpECS
{
    /// <summary>
    /// Base for all Entities.
    /// It's an interface just incase you want to write an Entity class
    /// instead of making an instance of BaseEntity or something.
    /// For example:
    ///     - Making a tile class for your game and you want to use your own TileWorld class.
    ///     - ... etc.
    /// </summary>
    public interface IEntity
    {
        List<IComponent> Components { get; set; }

        string Tag { get; set; }
        void AddComponent(IComponent c);
        T FindComponent<T>() where T : IComponent;
        dynamic FindComponent(string tag);
    }
}
