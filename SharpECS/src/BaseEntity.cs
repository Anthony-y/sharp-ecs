﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpECS
{
    /// <summary>
    /// The base class for IEntity.
    /// Can be used as a starting point for creating simple entities
    /// Use this, or write your own.
    /// You cannot inherit from this.
    /// If you need a custom entity, instead inherit from IEntity,
    /// and implement features yourself.
    /// </summary>
    public sealed class BaseEntity : IEntity
    {
        public List<IComponent> Components { get; set; }
        public string Tag { get; set; }

        public BaseEntity(string tag)
        {
            Tag = tag;
            Components = new List<IComponent>();
        }

        /// <summary>
        /// Simple implementation of IEntity's AddComponent.
        /// </summary>
        /// <param name="c"></param>
        public void AddComponent(IComponent c)
        {
            if (c.Owner == this) { Components.Add(c); }
        }

        /// <summary>
        /// Find component returns the first component on the entity of the type "T"
        /// if it cannot be found, it returns null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T FindComponent<T>() where T : IComponent
        {
            return Components.OfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Returns a dynamic value instead of a generic.
        /// This can be useful when, for example, the component you are trying to access
        /// is not in a C# scope. (I implemented this for myself because I wrote a component
        /// in IronPython and wanted to access it from C#.)
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public dynamic FindComponent(string tag)
        {
            var match = Components.FirstOrDefault(com => com.Tag.Equals(tag));
            return match;
        }
    }
}
