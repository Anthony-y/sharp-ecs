﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SharpECS
{
    /// <summary>
    /// An EntitySystem is the ONLY way to manipulate entities.
    /// Entity systems check to see if any entities in the specified entity world
    /// contain the component of type "T".
    /// It can them maniuplate these entities ONLY.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntitySystem<T> 
        : IGlobalSystem
        where T : IComponent
    // T is the type of component which this System will iterate through and perform logic on.
    {
        public List<IEntity> Entities           { get; set; }
        public EntityWorld EntityWorld          { get; set; }
        protected List<IEntity> CompatibleEntities { get; set; } = new List<IEntity>();
        public IEntity ComponentOwner { get; set; }

        public bool IsActive { get; set; }

        public EntitySystem(EntityWorld entityWorld)
        {
            Entities = entityWorld.Entities;
            GetAppropriate();

            if (CompatibleEntities.Count >= 1)
            {
                Initialize();
            } else Console.WriteLine("Could not create Entity System. No entities hold the compatible component."); IsActive = false;
        }

        /// <summary>
        /// Do all your initialization in here,
        /// NOT IN THE CONSTRUCTOR.
        /// Please.
        /// </summary>
        public virtual void Initialize()
        {
            if (!IsActive)
            {
                return;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!IsActive)
            {
                return;
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            if (!IsActive)
            {
                return;
            }
        }

        /// <summary>
        /// Gets all entities which have the compatible component of type "T"
        /// </summary>
        public void GetAppropriate()
        {
            foreach (var i in Entities)
            {
                if (i.FindComponent<T>() != null)
                {
                    CompatibleEntities.Add(i);
                    ComponentOwner = i;
                }
            }
        }
    }
}
