# Sharp ECS
(Formerly "component-engine3.0")

A nice and easy framework for building games with the Entity Component System design pattern in C# designed for use with XNA4 and MonoGame.

# Build and use
1. Download the code as .zip or pull from GitHub
2. Open the "SharpECS.sln" Visual Studio solution file (only tested with VS 2015 but should work with 2010+)
3. Make sure your Startup Project is on "SharpECS" and that your build config is on Release.
4. Hit Build -> Build SharpECSFNA
5. Add the .dll as a reference in your VS project/solution and include "using SharpECS;" at the top of your code
